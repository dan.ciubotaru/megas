 #include <vector>
#include <random>
#include <boost/units/systems/si.hpp>
#include <boost/units/systems/cgs.hpp>
#include <boost/units/systems/si/prefixes.hpp>
#include <betaboltz.hpp>
#include "megas/MicromegasDetector.hpp"


using namespace std;
using namespace dfpe;
using namespace boost::units;

MicromegasDetector::MicromegasDetector()
{   
    sizeX  = QtySiLength(20. * si::centi * si::meter);
    sizeY  = QtySiLength(20. * si::centi * si::meter);

    heightUpper = QtySiLength( 20. * si::milli * si::meter);
    heightDrift = QtySiLength(  5. * si::milli * si::meter);
    heightAmpli = QtySiLength(128. * si::micro * si::meter);
    
    cathodeVoltage = QtySiElectricPotential(-300. * si::volt);
    meshVoltage    = QtySiElectricPotential(   0. * si::volt);
    anodeVoltage   = QtySiElectricPotential( 550. * si::volt);
    
    updateFields();
}

const std::vector<int> MicromegasDetector::getVolumeIds() const
{
    return  
    {
        0, // Amplification volume
        1, // Drift volume
        2, // Upper volume
    };
}

int MicromegasDetector::getVolumeId(const VectorC3D<QtySiLength> &position) const
{

    if (abs(position.x) <= sizeX && abs(position.y) <= sizeY)
    {
        if (position.z >= QtySiLength())
        {
                if (position.z < heightAmpli)
                        return 0;
                else if (position.z < heightAmpli + heightDrift)
                        return 1;
                else if (position.z < heightAmpli + heightDrift + heightUpper)
                        return 2;
        }
    }
        return -1; // No volume
}

const BaseField& MicromegasDetector::getField(int volumeId) const
{
    switch (volumeId)
    {
    case 0:
        return fieldAmpli;
    case 1:
        return fieldDrift;
    case 2:
        return fieldUpper;
    default:
        throw runtime_error("Invalid volume Id");
    }
}

void MicromegasDetector::updateFields()
{
        fieldAmpli = UniformFieldClassic(VectorC3D<QtySiElectricField>(QtySiElectricField(), QtySiElectricField(), (anodeVoltage - meshVoltage) / heightAmpli));
        fieldDrift = UniformFieldClassic(VectorC3D<QtySiElectricField>(QtySiElectricField(), QtySiElectricField(), (meshVoltage - cathodeVoltage) / heightDrift));
        fieldUpper = UniformFieldClassic(VectorC3D<QtySiElectricField>(QtySiElectricField(), QtySiElectricField(), -cathodeVoltage / heightUpper));
}
        
const GasMixture& MicromegasDetector::getGasMixture(int volumeId) const
{
        return gas;
}

void MicromegasDetector::getDriftIntersectionCoordinates(
        const VectorC3D<QtySiLength>& gunTarget,
        const VectorC3D<QtySiLength>& gunDirection,
        VectorC3D<QtySiLength>& intersectionUpper,
        VectorC3D<QtySiLength>& intersectionLower) const
{
        
        VectorC3D<QtySiLength> plane1Normal(0. * si::meter, 0. * si::meter, -1. * si::meter);
        VectorC3D<QtySiLength> plane2Normal(0. * si::meter, 0. * si::meter, -1. * si::meter);
        
        VectorC3D<QtySiLength> plane1Point;
        VectorC3D<QtySiLength> plane2Point;
        plane1Point.z = heightAmpli + heightDrift;
        plane2Point.z = heightAmpli;
        
        QtySiDimensionless d1 = (plane1Point - gunTarget).dot(plane1Normal) / gunDirection.dot(plane1Normal);
        QtySiDimensionless d2 = (plane2Point - gunTarget).dot(plane2Normal) / gunDirection.dot(plane2Normal);
        
        intersectionUpper = d1 * gunDirection + gunTarget;
        intersectionLower = d2 * gunDirection + gunTarget;
}

vector<VectorC3D<QtySiLength>> MicromegasDetector::getRandomIntPoints(
        default_random_engine &generator,
        const VectorC3D<QtySiLength>& gunTarget,
        const VectorC3D<QtySiLength>& gunDirection,
    dfpe::QtySiWavenumber nrOfPoints,
    dfpe::QtySiPlaneAngle gunAngle) const
{
    assert(!gunDirection.isNull());

    VectorC3D<QtySiLength> point1;
    VectorC3D<QtySiLength> point2;

    getDriftIntersectionCoordinates(gunTarget, gunDirection, point1, point2);

    uniform_real_distribution<double> distribution(0.0,1.0);
    
    int n = round(nrOfPoints * getHeightDrift() / cos(gunAngle));

        vector<VectorC3D<QtySiLength>> points;
    for (int i = 0; i < n; ++i)
    {
                double random = distribution(generator);
                VectorC3D<QtySiLength> interactionPoint = point1 + (point2 - point1) * random;
                
                if (getVolumeId(interactionPoint) >= 0) 
                points.push_back(interactionPoint);     
    }
        return points;
}
