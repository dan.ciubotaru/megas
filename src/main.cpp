#include <random>
#include <boost/units/quantity.hpp>
#include <boost/units/systems/si.hpp>
#include <boost/units/systems/si/prefixes.hpp>
#include <boost/units/systems/si/codata_constants.hpp>
#include <boost/units/systems/angle/degrees.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <betaboltz.hpp>
#include "megas/MicromegasSetup.hpp"
#include "megas/MicromegasDetector.hpp"
#include "megas/SimulationLimiter.hpp"
#include "bethy/Bethy.hpp"
#include <CLI/CLI.hpp>

using namespace dfpe;
using namespace boost::units;
using namespace boost::units::si::constants;
using namespace std;
using namespace CLI;

int main(int argc, const char** argv)
{
    CLI::App app{"Drifter - Gas Drift Velocity"};

    double detectorAngleArg = 0;
    app.add_option("-a, --angle", detectorAngleArg, "Detector angle [deg]")->capture_default_str();
    
    std::optional<int> events;
    app.add_option("-e, --events", events, "Number of events")->capture_default_str();
        
    std::optional<int> eventStart;
    app.add_option("--event-start", eventStart, "Start the event numbering from a given number")->capture_default_str();
    
    std::optional<int> eventEnd;
    app.add_option("--event-end",   eventEnd,   "Last event number to simulate")->capture_default_str();
    
    
    string bulletArg;
    app.add_option("--bullet", bulletArg, "Bullet name or PDG Id number")->required();

    auto groupBulletProperties = app.add_option_group("bullet properties", "Bullet properties");
    std::optional<double> bulletEnergyArg;
    auto bulletOption1 = app.add_option("--energy", bulletEnergyArg, "Bullet energy [keV]")->capture_default_str();
    std::optional<double> bulletMomentumArg;
    auto bulletOption2 = app.add_option("--momentum", bulletMomentumArg, "Bullet momentum [keV/c]")->capture_default_str();
    bulletOption1->excludes(bulletOption2);
    bulletOption2->excludes(bulletOption1);
    groupBulletProperties->add_option(bulletOption1);
    groupBulletProperties->add_option(bulletOption2);

    int run = 0;
    app.add_option("-r, --run", run, "Current run ID")->capture_default_str();
    
    int threads = 0;
    app.add_option("-j, --jobs", threads, "Number of concurrent threads")->capture_default_str();
    
    double cathodeVoltageArg = -300.;
    double meshVoltageArg    =    0.;
    double anodeVoltageArg   =  550.;
    
    auto groupVoltages          = app.add_option_group("voltages", "Voltages in the detector");
    groupVoltages->add_option("--cathode-voltage", cathodeVoltageArg, "Cathode voltage [V]")->capture_default_str();
    groupVoltages->add_option("--mesh-voltage",    meshVoltageArg,    "Mesh voltage [V]")->capture_default_str();
    groupVoltages->add_option("--anode-voltage",   anodeVoltageArg,   "Anode voltage [V]")->capture_default_str();
    
    vector<string> gases = {"Ar", "CO2"};
    app.add_option("-g, --gases",   gases,   "Gas molecules")->capture_default_str();
    
    vector<double> ratios = {93., 7.};
    app.add_option("-x, --ratios",   ratios,   "Gas mixture ratios")->capture_default_str();
    
    vector<string> databases = {"ela:BSR/BSR-500|ine:Biagi8/Ar", "ela:Phelps/CO2|ine:Biagi8/CO2"};
    app.add_option("-d, --databases",   databases,   "Gas databases")->capture_default_str();
    
    double pressureArg = 101.325;
    app.add_option("-p, --pressure", pressureArg, "Gas pressures [kPa]")->capture_default_str();
    
    bool full = false;
    app.add_flag("--full", full, "Show the collisions and not only creation/destruction of particles")->capture_default_str();
    
    bool verbose = false;
    app.add_flag("-v, --verbose", verbose, "Show the creation/destruction of particles")->capture_default_str();

	bool append = false;
    app.add_flag("--append", append, "Append the results to existing files making easier concatenating with files from previous executions")->capture_default_str();

    bool disableSecondaries = false;
    app.add_flag("--disable-secondaries", disableSecondaries, "Disable the creation of secondary particles due to ionizations")->capture_default_str();

    string output;
    app.add_option("output", output, "Output basename")->required();
    
    try { app.parse(argc, argv); } catch (const CLI::ParseError &e) { return app.exit(e); }

    int simulatingEvents = 1;

    if (events.has_value() && eventStart.has_value() && eventEnd.has_value())
    {
         if (eventEnd.value() - eventStart.value() + 1 != events.value())
         {
             cerr << "ERROR: --event-start and --event-end does not math --events flag" << endl;
             return -1;
         }
         simulatingEvents = events.value();
    }
    else if (!events.has_value() && eventStart.has_value() && eventEnd.has_value())
    {
        simulatingEvents = max(eventEnd.value() - eventStart.value() + 1, 0);
    }
    else if (events.has_value() && !eventEnd.has_value())
    {
        simulatingEvents = events.value();
    }
    else if (!events.has_value() && !eventStart.has_value() && eventEnd.has_value())
    {
        simulatingEvents = eventEnd.value() + 1;
    }

    QtySiPlaneAngle detectorAngle(detectorAngleArg * degree::degree);


    std::optional<ParticleSpecie> bulletFound = PDGTable::instance.get(bulletArg);

    if (!bulletFound.has_value())
        throw std::runtime_error("Unable to find the particle '" + bulletArg + "' in the PDG database. Try to use the PDG Id instead.");

    ParticleSpecie bullet = bulletFound.value();

    optional<QtySiEnergy>   bulletEnergy;
    optional<QtySiMomentum> bulletMomentum;

    if (!bulletEnergyArg.has_value() && !bulletMomentumArg.has_value() )
        throw std::runtime_error("One of bullet 'energy' or 'momentum' shall be provided.");
    else if (bulletEnergyArg.has_value())
        bulletEnergy = *bulletEnergyArg * 1e3 * electronvolt;
    else if (bulletMomentumArg.has_value())
        bulletMomentum = *bulletMomentumArg * 1e3 * electronvolt / codata::c;
    else
        assert(false);
    
    QtySiPressure   pressure(pressureArg * si::kilo * si::pascal);

    QtySiElectricPotential cathodeVoltage(cathodeVoltageArg * si::volt);
    QtySiElectricPotential meshVoltage(meshVoltageArg * si::volt);
    QtySiElectricPotential anodeVoltage(anodeVoltageArg * si::volt);
    

   
    default_random_engine generator(time(nullptr));

    BetaboltzSimple beta;
    
    if (threads > 0)
        beta.setNumThreads(threads);
        
    beta.setNextRunId(run);

    if (eventStart.has_value())
        beta.setBaseEventId(eventStart.value());

    BetaboltzFlags flags;

    flags.setDisableCheckMonoatomic(true);
    if (disableSecondaries)
    {
        flags.setEnableIonizations(false);
        flags.setEnableAttachments(false);
    }

    beta.setFlags(flags);
    
    // Set the total density of the gas mixture
    QtySiDensity      density = 2.6867811e25 / (si::meter * si::meter * si::meter) * pressure / QtySiPressure(101.325 * si::kilo * si::pascal);

    double sumRatios = std::accumulate(ratios.begin(), ratios.end(), 0.);

    // Here we describe the gas mixture
    GasMixture gas;
    for (size_t i = 0; i < gases.size(); i++)
    {
        QtySiDensity densityRelative (ratios.at(i) / sumRatios * density);
        gas.addComponent(gases.at(i),  densityRelative);
        beta.enableProcess("e", gases.at(i), databases.at(i));
    }


    QtySiWavenumber interactions;

    if (bulletEnergy.has_value())
        interactions = Bethy(gas).getMeanInteractions(bullet, *bulletEnergy);
    else if (bulletMomentum.has_value())
        interactions = Bethy(gas).getMeanInteractions(bullet, *bulletMomentum);
    else
        assert(false);

    auto exportHandler = make_shared<ExportCollisionsHandler>(output, append);
    if (!full)
    {
        exportHandler->setColumnsFilter("run_id, event_id, volume_id, particle_id, process, time, position");
        exportHandler->setShowCollisions(false);
    }

    beta.addHandler(exportHandler);

    auto printHandler = make_shared<PrintProgressHandler>(verbose ? PrintProgressVerbosity::EVENT_LEVEL : PrintProgressVerbosity::NONE_LEVEL);
    beta.addHandler(printHandler);
    
    auto limiter = make_shared<SimulationLimiter>();
    beta.addLimiter(limiter);

    auto detectors = make_shared<MicromegasSetup>();

    auto detectorA = make_shared<MicromegasDetector>();
    detectorA->setGas(gas);
    VectorC3D<QtySiLength> positionDetA(0.0 * si::meter, 0.0 * si::meter, 0.5 * si::meter);

    // Using proper intrinsic Euler rotation x-y-x
    EulerRotation3D<QtySiLength> rotationDetA(QtySiPlaneAngle(), detectorAngle, QtySiPlaneAngle(), RotationMode::INTRINSIC, RotationAxis::X, RotationAxis::Y, RotationAxis::X);
    detectors->addDetector(detectorA, positionDetA, rotationDetA);
    
    detectorA->setCathodeVoltage(cathodeVoltage);
    detectorA->setMeshVoltage(meshVoltage);
    detectorA->setAnodeVoltage(anodeVoltage);

    auto detectorB = make_shared<MicromegasDetector>();
    detectorB->setGas(gas);
    VectorC3D<QtySiLength> positionDetB(0.0 * si::meter, 0.0 * si::meter, -0.5 * si::meter);

    // Using proper intrinsic Euler rotation x-y-x
    EulerRotation3D<QtySiLength> rotationDetB(QtySiPlaneAngle(), detectorAngle, QtySiPlaneAngle(), RotationMode::INTRINSIC, RotationAxis::X, RotationAxis::Y, RotationAxis::X);
    detectors->addDetector(detectorB, positionDetB, rotationDetB);
    
    detectorB->setCathodeVoltage(cathodeVoltage);
    detectorB->setMeshVoltage(meshVoltage);
    detectorB->setCathodeVoltage(cathodeVoltage);

    beta.setDetector(detectors);

    // Set the gun target: by default (0,0,0)
    VectorC3D<QtySiLength> gunTarget;

    // Set the gun direction:
    VectorC3D<QtySiLength> gunDirection(0. * si::meter, 0. * si::meter, -1. * si::meter);
    //gunDirection = gunDirection.rotateY(gunAngle); // We rotate the detectors, not the gun

    vector<ParticleState> initialStates;

    // Set the number of primary interaction inside the sensitive area
    for(const VectorC3D<QtySiLength> &point: detectors->getRandomIntPoints(generator, gunTarget, gunDirection, interactions, detectorAngle))
    {
        ParticleState state;
        state.position = point;
        initialStates.push_back(state);
    }

    // remove the extension for the outout parameter
    string raw_name = output.substr(0, output.find_last_of("."));
    // Saving the configuration of the detectors for later use
    detectors->saveDetectorInfoToCSV(raw_name + "_detector_info" + ".csv");

    ElectronSpecie electronSpecie;
    beta.execute(electronSpecie, initialStates, simulatingEvents);

    cout << "Simulation efficiency: " << beta.getStatsEfficiency() << endl;
}
