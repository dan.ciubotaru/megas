#include "megas/SimulationLimiter.hpp"
using namespace dfpe;

bool SimulationLimiter::isOver(const QtySiTime &currentTime, int volumeId, const Specie &particleSpecie, const ParticleState &state) const 
{
    return volumeId % 10 != 1;
}   
