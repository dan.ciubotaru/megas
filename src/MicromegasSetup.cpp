#include "megas/MicromegasSetup.hpp"
#include <iostream>
#include <fstream>
using namespace std;
using namespace dfpe;
using namespace boost::units;

MicromegasSetup::MicromegasSetup()
{

}

vector<VectorC3D<QtySiLength>> MicromegasSetup::getRandomIntPoints(
    default_random_engine &generator,
	const VectorC3D<QtySiLength>& gunTarget,
	const VectorC3D<QtySiLength>& gunDirection,
    dfpe::QtySiWavenumber intPointsPerDistance,
    dfpe::QtySiPlaneAngle gunAngle) const
{
    assert(!gunDirection.isNull());

    vector<VectorC3D<QtySiLength>> points;

    for (size_t i = 0; i < detectors.size(); i++)
    {
        const MicromegasDetector& detector = *dynamic_pointer_cast<MicromegasDetector>( detectors[i]);

        const auto& frame = frames[i];


        const VectorC3D<QtySiLength> gunTargetLocal    = frame->forward(gunTarget);
        const VectorC3D<QtySiLength> gunDirectionLocal = frame->forward(gunDirection);

        vector<VectorC3D<QtySiLength>> localPoints = detector.getRandomIntPoints(generator, gunTargetLocal, gunDirectionLocal, intPointsPerDistance, gunAngle);

        for(const VectorC3D<QtySiLength> &localPoint: localPoints)
        {
            points.push_back(frame->backward(localPoint));
        }
    }
    return points;
}

void MicromegasSetup::saveDetectorInfoToCSV(std::string filename) const
{


    ofstream file;
    file.open (filename);
    file << "detector_id, size_x, size_y, height_upper, height_drift, height_ampli, pos_x, pos_y, pos_z, axis_x_x, axis_x_y, axis_x_z, axis_y_x, axis_y_y, axis_y_z, xis_z_x, axis_z_y, axis_z_z" << endl;

    for (size_t i = 0; i < frames.size(); ++i)
    {
        const MicromegasDetector &detector = *dynamic_pointer_cast<MicromegasDetector>(detectors[i]);
        const auto& frame = frames[i];

        // This will return the translation done
        const auto& translation  = frame->backward(VectorC3D<QtySiLength>());

        QtySiLength one (1. * si::meter);
        QtySiLength zero;
        VectorC3D<QtySiLength> axisX(one,  zero, zero);
        VectorC3D<QtySiLength> axisY(zero,  one, zero);
        VectorC3D<QtySiLength> axisZ(zero, zero,  one);

        // This will return the rotation done
        const auto& destAxisX = frame->backwardRotOnly(axisX);
        const auto& destAxisY = frame->backwardRotOnly(axisY);
        const auto& destAxisZ = frame->backwardRotOnly(axisZ);

        file << i << ",";
        file << detector.getSizeX() << ",";
        file << detector.getSizeY() << ",";
        file << detector.getHeightUpper() << ",";
        file << detector.getHeightDrift() << ",";
        file << detector.getHeightAmpli() << ",";
        file << translation.x << ",";
        file << translation.y << ",";
        file << translation.z << ",";
        file << destAxisX.x  << ",";
        file << destAxisX.y  << ",";
        file << destAxisX.z  << ",";
        file << destAxisY.x << ",";
        file << destAxisY.y << ",";
        file << destAxisY.z << ",";
        file << destAxisZ.x << ",";
        file << destAxisZ.y << ",";
        file << destAxisZ.z << endl;
    }
    file.close();
}

