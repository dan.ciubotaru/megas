#pragma once
#include <betaboltz.hpp>
#include <random>

class MicromegasDetector: public dfpe::BaseDetector
{
public:
    MicromegasDetector();
    virtual ~MicromegasDetector() {};

    virtual const std::vector<int> getVolumeIds() const override;

    virtual int getVolumeId(const dfpe::VectorC3D<dfpe::QtySiLength> &position) const override;
    virtual const dfpe::GasMixture& getGasMixture(int volumeId) const override;

    virtual const dfpe::BaseField& getField(int volumeId) const override;

protected:

    dfpe::GasMixture gas;

    dfpe::QtySiElectricPotential meshVoltage;
    dfpe::QtySiElectricPotential cathodeVoltage;
    dfpe::QtySiElectricPotential anodeVoltage;

    dfpe::UniformFieldClassic fieldUpper;
    dfpe::UniformFieldClassic fieldDrift;
    dfpe::UniformFieldClassic fieldAmpli;

    dfpe::QtySiLength sizeX;
    dfpe::QtySiLength sizeY;

    dfpe::QtySiLength heightUpper;
    dfpe::QtySiLength heightDrift;
    dfpe::QtySiLength heightAmpli;

public:
    void setGas(const dfpe::GasMixture& gas)       { MicromegasDetector::gas = gas; }

    void setSizeX(const dfpe::QtySiLength& length) { MicromegasDetector::sizeX = length; }
    void setSizeY(const dfpe::QtySiLength& length) { MicromegasDetector::sizeY = length; }

    void setHeightUpper(const dfpe::QtySiLength& height)  { MicromegasDetector::heightUpper = height; updateFields();}
    void setHeightDrift(const dfpe::QtySiLength& height)  { MicromegasDetector::heightDrift = height; updateFields();}
    void setHeightAmpli(const dfpe::QtySiLength& height)  { MicromegasDetector::heightAmpli = height; updateFields();}

    void setMeshVoltage   (const dfpe::QtySiElectricPotential& voltage) { MicromegasDetector::meshVoltage    = voltage; updateFields();};
    void setCathodeVoltage(const dfpe::QtySiElectricPotential& voltage) { MicromegasDetector::cathodeVoltage = voltage; updateFields();};
    void setAnodeVoltage(const dfpe::QtySiElectricPotential& voltage)   { MicromegasDetector::anodeVoltage = voltage; updateFields();};

    const dfpe::UniformFieldClassic &getFieldUpper() const { return fieldUpper; }
    const dfpe::UniformFieldClassic &getFieldDrift() const { return fieldDrift; }
    const dfpe::UniformFieldClassic &getFieldAmpli() const { return fieldAmpli; }
    const dfpe::QtySiLength &getSizeX() const { return sizeX; }
    const dfpe::QtySiLength &getSizeY() const { return sizeY; }
    const dfpe::QtySiLength &getHeightUpper() const { return heightUpper; }
    const dfpe::QtySiLength &getHeightDrift() const { return heightDrift; }
    const dfpe::QtySiLength &getHeightAmpli() const { return heightAmpli; }

    std::vector<dfpe::VectorC3D<dfpe::QtySiLength>> getRandomIntPoints(
        std::default_random_engine &generator,
        const dfpe::VectorC3D<dfpe::QtySiLength>& gunTarget,
        const dfpe::VectorC3D<dfpe::QtySiLength>& gunDirection,
        dfpe::QtySiWavenumber nrOfPoints,
        dfpe::QtySiPlaneAngle gunAngle
        ) const;

protected:
    void getDriftIntersectionCoordinates(
        const dfpe::VectorC3D<dfpe::QtySiLength>& gunTarget,
        const dfpe::VectorC3D<dfpe::QtySiLength>& gunDirection,
        dfpe::VectorC3D<dfpe::QtySiLength>& intersectionUpper,
        dfpe::VectorC3D<dfpe::QtySiLength>& intersectionLower) const;

    void updateFields();
};
