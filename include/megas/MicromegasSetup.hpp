# pragma once
#include <random>
#include <vector>
#include <betaboltz.hpp>
#include <megas/MicromegasDetector.hpp>

class MicromegasSetup : public dfpe::ComplexDetector {
public:
    MicromegasSetup();
    virtual ~MicromegasSetup() {};


    void saveDetectorInfoToCSV(std::string filename) const;

    std::vector<dfpe::VectorC3D<dfpe::QtySiLength>> getRandomIntPoints(
        std::default_random_engine &generator,
        const dfpe::VectorC3D<dfpe::QtySiLength>& gunTarget,
        const dfpe::VectorC3D<dfpe::QtySiLength>& gunDirection,
        dfpe::QtySiWavenumber intPointsPerDistance,
        dfpe::QtySiPlaneAngle gunAngle
        ) const;
};
