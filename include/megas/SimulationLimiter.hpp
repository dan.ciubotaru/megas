#pragma once
#include <betaboltz.hpp>

class SimulationLimiter : public dfpe::BaseBulletLimiter
{

public:
    virtual bool isOver(const dfpe::QtySiTime &currentTime, int volumeId, const dfpe::Specie &particleSpecie, const dfpe::ParticleState &state) const override;
};
